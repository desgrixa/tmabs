from gensim.corpora.dictionary import Dictionary
from gensim.models import LdaModel
from gensim.models import CoherenceModel
import pandas as pd
import numpy as np

import plotly.express as px
import plotly.graph_objects as go

from stqdm import stqdm
stqdm.pandas()

from tmabs.info_retrieval.bm25 import BM25
from tmabs.preprocessing import model_data_to_df, preprocess_topics, dynamic_topic_modelling
from tmabs.visualize.topics import plotly_topic_visualization, plot_3d, visualize_tot, visualize_barchart

import streamlit as st


def query_bm(df, query):
    """
    Query the BM25 model to get the BM25 scores for each document.
    """
    corpus = df.abstract
    tok_corpus = df.cleaned_abstract
    bm25 = BM25(tok_corpus)

    query = query.split()

    scores = bm25.get_scores(query)

    best_docs = sorted(range(len(scores)), key=lambda i: scores[i], reverse=True)
    # for i, b in enumerate(tqdm(best_docs)):
    #     if scores[b] > 0:
    #         # print(f"rank {i+1}, score = {scores[b]}, row: {b} \n {corpus[b][:100]}")
    best_docs = [b for b in best_docs if scores[b] > 0]
    df_best_docs = df.iloc[best_docs]
    df_best_docs.reset_index(inplace=True, drop=True)
    return df[:10000]

def compute_coherence_values(corpus, dictionary, top_nt = 20):
   topics = []
   score = []
   for i in stqdm(range(10,top_nt)):
      lda_model = LdaModel(  corpus=corpus, 
                                 id2word=dictionary, 
                                 iterations=10, 
                                 num_topics=i, 
                                 passes=5, 
                                 random_state=100,
                                 eta ='auto',
                                 eval_every=None
                                 )
      cm = CoherenceModel(model=lda_model, texts = df["n_gram_cleaned_abstract"], corpus=corpus, dictionary=dictionary, coherence='c_v')
      topics.append(i)
      score.append(cm.get_coherence())

      #plot the coherence score
      fig = go.Figure(data=[go.Scatter(x=topics, y=score)])
      fig.update_layout(title='Coherence Score vs Number of Topics', xaxis_title='Number of Topics', yaxis_title='Coherence Score')
      
   return fig


def lda_model(corpus, dictionary, num_topics):
    lda_model = LdaModel(  corpus=corpus, 
                              id2word=dictionary, 
                              iterations=10, 
                              num_topics=num_topics, 
                              passes=5, 
                              random_state=100,
                              )

    return lda_model

def count_words_in_abstract(df):
    """
    Counts the number of words in the abstract
    """
    df['word_count'] = df['abstract'].apply(lambda x: len(x.split()))
    return df

# @st.experimental_memo
@st.cache
def load_df():
    return pd.read_parquet(path="s3://media-offload-wordpress/data_test/egu_preprocessed_ngrams_min.parquet", engine='pyarrow', storage_options={"anon": True})

df = load_df()

st.title('Search EGU General Assembly Conference Abstracts with tmabs')

form = st.form("my_form")
query = form.text_input('Your query: ')
num_topics = form.slider('Choose numner of topics', 0, 25, 10)

# Every form must have a submit button.
submitted = form.form_submit_button("Submit")
if submitted:
    df = query_bm(df, query)
    dictionary = Dictionary(df.n_gram_cleaned_abstract)
    dictionary.filter_extremes(no_below=5, no_above=0.5)
    
    df_plt = count_words_in_abstract(df)

    fig = px.histogram(df_plt, x="year", template="plotly_white", title="Abstracts count by date")
    fig.update_xaxes(categoryorder="category ascending", title="Date").update_yaxes(title="Number of Abstracts")
    fig.update_layout(width=1200, height=500)
    st.plotly_chart(fig)

    fig = px.histogram(
        df_plt,
        x="word_count",
        template="plotly_white",
        title="Abstracts count by length"
    )
    fig.update_xaxes(
        categoryorder="total descending",
        title="Number of words",
        range=[0, 1000]
    ).update_yaxes(title="Number of abstracts")
    fig.update_layout(width=1200, height=500)
    st.plotly_chart(fig)

    top_authors = (
    df_plt.groupby("first_author")["year"]
    .count()
    .sort_values(ascending=False)[:30]
    .index
    )
    top_authors_df = df_plt[df_plt["first_author"].isin(top_authors)]

    fig = px.histogram(
            top_authors_df,
            x="first_author",
            template="plotly_white",
            title="Author count",
        )
    fig.update_xaxes(categoryorder="total descending").update_yaxes(
        title="Number of EGU publications"
    )
    fig.update_layout(width=1200, height=500)
    st.plotly_chart(fig)

    most_common_words = dictionary.most_common(100)
    df_most_common_words = pd.DataFrame(most_common_words, columns=['word', 'count'])
    fig = px.bar(df_most_common_words, x='word', y='count', color='count')
    fig.update_layout(width=1200, height=500)
    st.plotly_chart(fig)

    corpus = [dictionary.doc2bow(abs) for abs in stqdm(df.n_gram_cleaned_abstract)]
    
    lda_model = lda_model(corpus, dictionary, num_topics)

    st.plotly_chart(visualize_barchart(lda_model))

    df = model_data_to_df(df, lda_model, corpus)
    df = dynamic_topic_modelling(df, lda_model)

    
    df_topics_2d = preprocess_topics(lda_model, corpus, dictionary)
    st.plotly_chart(plotly_topic_visualization(df_topics_2d, width=1200, height=800))

    df_topics_3d = preprocess_topics(lda_model, corpus, dictionary, n_components=3)
    st.plotly_chart(plot_3d(df_topics_3d, width=1200, height=800))

    st.plotly_chart(visualize_tot(df, width=1200, height=800))

    for i in range(0, num_topics):
        df_topic = df[df["topic_id"] == i]
        df_topic = df_topic.sort_values(by=["doc_topic_probabily"], ascending=False)
        df_top3_topic = df_topic[['first_author', 'title_str', 'abstract', 'doc_topic_probabily', 'topic_keywords']][:3]
        st.write(f"Topic {i}")
        st.json(df_top3_topic.to_json(orient='records'))


    @st.cache
    def convert_df(df):
        return df.to_csv().encode('utf-8')

    csv = convert_df(df)

    st.download_button(
        "Press to Download df",
        csv,
        "df.csv",
        "text/csv",
        key='download-csv'
    )

