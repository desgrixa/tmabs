# TMABS

Topic modelling on abstracts


## Getting started

1. Create python virtual environment: `python3 -m venv tmabs_env`
2. Once you’ve created a virtual environment, you may activate it.
   - On Windows, run: `tmabs_env\Scripts\activate.bat`
   - On Unix or MacOS, run: `source tmabs_env/bin/activate`
3. pip version should be >= 22.0 `pip install --upgrade pip`
4. Install tmabs: `pip install -e .` 

## Streamlit application

Check out the **demo**: https://share.streamlit.io/desgrixa/abs-zero-shot-mining/main

```
streamlit run streamlit_app/app.py
```