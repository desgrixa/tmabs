import requests
import pandas as pd
import re
import string
from tqdm import tqdm

"""
## Query Parameters
All query parameters passed in the search URL must be UTF-8, URL-encoded strings. 
Due to the requirements of the authentication library used for validating requests, 
most non-ASCII characters that appear in the URL need to be encoded; for example, the double quote character (") 
should be encoded as %22. 

#### q
**Required** The search query. This should be a UTF-8, URL-encoded string of <=1000 characters. `q` accepts both fields 
(`title:exoplanets`) and unfielded (`exoplanets`) searches.

#### rows 
The number of results to return. The default is 10 and the maximum is 2000.

#### start
The starting point for returned results, used for pagination. The default is 0. 
To return the next page of results, set `start` equal to the value of `start` from the previous request, 
plus the number of results returned in the previous request. 
For the default values, set `start=10` to return the second page of results.

#### fl
The list of fields to return. The value should be a comma separated list of field names, e.g. 
`fl=bibcode,author,title`. The default is the document id (`fl=id`). A non-exhaustive list of available fields [is shown below](#fields).

#### fq
Filters the list of search results. 
The syntax is the same as that for the `q` parameter. Adding search parameters via the `fq` parameter can speed up search results, 
as it searches only the results returned by the search entered via the `q` parameter, not the entire index. 
This parameter may be used more than once in a single search URL.

#### sort
The sorting field and direction to be used when returning results. 
The format requires both the field to sort on (see [the list below](#fields) and the direction, either `asc` or `desc` (ascending or descending). 
For example, an appropriately formatted sort parameter is `sort=citation_count+desc`. 
The default sort method is the relevancy score as calculated by the search engine. 
Other useful fields to sort on may be `date`, `read_count`, `first_author`, or `bibcode`.

## Fields
This is a non-exhaustive list of fields available for searching via the API. A more comprehensive list is available 
here - https://adsabs.github.io/help/search/comprehensive-solr-term-list). These fields can be used with the `fl` and `sort` parameters. 

Note: some fields, such as body, can be searched but not returned via `fl` or sorted on. 
Also, multivalued fields, such as author, cannot be used for sorting.

* `abstract` - the abstract of the record
* `ack` - the acknowledgements section of an article
* `aff` - an array of the authors' affiliations
* `alternate_bibcode` - list of alternate bibcodes for a single record; if a published record also has an arXiv version, the bibcode associated with the arXiv version is here
* `alternate_title` - list of alternate titles for a single record (usually if they are in multiple languages)
* `arxiv_class` - the arXiv class the pre-print was submitted to
* `author` - an array of the author names associated with the record
* `bibcode` - the canonical ADS bibcode identifier for this record
* `bibgroup` - the bibliographic groups that the bibcode has been associated with
* `bibstem` - the abbreviated name of the journal or publication, e.g., *ApJ*.
* `body`\* - the full text content of the article
* `citation_count` - number of citations the item has received
* `copyright` - the copyright applied to the article
* `data` - the list of sources that have data related to this bibcode
* `date` - the machine-readable version of `pubdate`, useful for sorting
* `database` - database the record is associated with (astronomy, physics, or general). By default, all three databases are searched; to limit to astronomy articles, add `fq=database:astronomy` to the URL
* `doi`-  the digital object identifier of the article
* `doctype` - the type of document it is (see [here](https://adsabs.github.io/help/search/search-syntax) for a list of doctypes)
* `first_author` - the first author of the article
* `grant` - the list of grant IDs and agencies noted within an article
* `id` - a **non-persistent** unique integer for this record, used for fast look-up of a document
* `identifier` - an array of alternative identifiers for the record. May contain alternate bibcodes, DOIs and/or arxiv ids.
* `indexstamp` - time at which the document was (last) indexed
* `issue` - issue the record appeared in
* `keyword` - an array of normalized and un-normalized keyword values associated with the record
* `lang`\* - the language of the article's title
* `orcid_pub` - ORCiD iDs supplied by publishers
* `orcid_user` - ORCiD iDs supplied by knonwn users in the ADS
* `orcid_other` - ORCiD iDs supplied by anonymous users in the ADS
* `page` - starting page
* `property` - an array of miscellaneous flags associated with the record (see [here](https://adsabs.github.io/help/search/search-syntax) for a list of properties
* `pub` - the canonical name of the publication the record appeared in
* `pubdate` - publication date in the form YYYY-MM-DD (DD value will always be "00"). This field is stored as a human-readable string, so is useful for being returned via `fl`, but not for sorting (see `date`)
* `read_count` - number of times the record has been viewed within in a 90-day windows (ads and arxiv)
* `title` - the title of the record
* `vizier` - the subject tags given to the article by VizieR
* `volume` - volume the record appeared in
* `year` - the year the article was published

"""

token="0gd0Jl9Vky6q0RfWOm4dbfNk2fwr5L1PkpsMAB0Z"


def get_ads_data(query="pub%3AEGU%20General%20Assembly&fl=*"):
    """
    Retrieve data from ADS.
    """
    rows = 2000 # fetch 2000 records at a time
    start = 0  # start with the first result

    # this is the pagination - the while loop will automatically stop once we've fetched all docs
    all_docs = []
    docs = True
    while docs:
        results = requests.get(f"https://api.adsabs.harvard.edu/v1/search/query?q={query}" \
                            f"&rows={rows}" \
                            f"&start={start}", \
                            headers={'Authorization': 'Bearer ' + token})
        try:
            docs = results.json()['response']['docs']
        except KeyError:
            print('No docs found')
            break
        all_docs.append(docs)
        start += rows # increment the start value to move to the next page of results

    return pd.concat([pd.DataFrame(d) for d in tqdm(all_docs)])