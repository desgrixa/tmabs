import spacy
import pandas as pd
from tqdm import tqdm
import re
import string
import nltk
from tmabs.info_retrieval.bm25 import BM25
import gensim
import numpy as np
from scipy.sparse import issparse
from umap import UMAP
from sklearn.preprocessing import MinMaxScaler

from numba.core.errors import NumbaDeprecationWarning, NumbaPendingDeprecationWarning
import warnings
warnings.simplefilter('ignore', category=NumbaDeprecationWarning)
warnings.simplefilter('ignore', category=NumbaPendingDeprecationWarning)

tqdm.pandas()

#loading the english language small model of spacy
nlp = spacy.load('en_core_web_sm')

sw_spacy = nlp.Defaults.stop_words
stop_word_list = sw_spacy.copy()

def basic_preprocessing(df, lemmatize = True, removal= ['ADV','PRON','CCONJ','PUNCT','PART','DET','ADP','SPACE', 'NUM', 'SYM'], n_processes = 1):
    """
    Preprocessing pipeline for the data. The pipeline includes:
    1. Tokenization 
    2. Removing stopwords   
    3. Lemmatization (optional)
    4. Removing punctuation
    5. Removing numbers
    6. Lowercasing

    spaCy maps all language-specific part-of-speech tags to a small, fixed set of word type tags following the Universal Dependencies scheme. 
    The universal tags do not code for any morphological features and only cover the word type. They are available as the Token.pos and Token.pos_ attributes.

    .pos_ tag list:

        The docs list the following coarse-grained tags used for the pos and pos_ attributes:

        ADJ: adjective, e.g. big, old, green, incomprehensible, first
        ADP: adposition, e.g. in, to, during
        ADV: adverb, e.g. very, tomorrow, down, where, there
        AUX: auxiliary, e.g. is, has (done), will (do), should (do)
        CONJ: conjunction, e.g. and, or, but
        CCONJ: coordinating conjunction, e.g. and, or, but
        DET: determiner, e.g. a, an, the
        INTJ: interjection, e.g. psst, ouch, bravo, hello
        NOUN: noun, e.g. girl, cat, tree, air, beauty
        NUM: numeral, e.g. 1, 2017, one, seventy-seven, IV, MMXIV
        PART: particle, e.g. ’s, not,
        PRON: pronoun, e.g I, you, he, she, myself, themselves, somebody
        PROPN: proper noun, e.g. Mary, John, London, NATO, HBO
        PUNCT: punctuation, e.g. ., (, ), ?
        SCONJ: subordinating conjunction, e.g. if, while, that
        SYM: symbol, e.g. $, %, §, ©, +, −, ×, ÷, =, :), 😝
        VERB: verb, e.g. run, runs, running, eat, ate, eating
        X: other, e.g. sfpksdpsxmsa
        SPACE: space, e.g.

    https://spacy.io/api/token#attributes
    https://spacy.io/api/data-formats



    Parameters
    ----------
    df : pandas dataframe
        Dataframe containing the abstracts.

    lemmatize : bool
        If True, lemmatizes the abstracts.

    removal : list
        List of POS tags to be removed from the abstracts.

    n_processes : int
        Number of processes to be used for the preprocessing.

    Returns
    -------
    df : pandas dataframe

    """
    tokens = []

    if lemmatize:
        for abs in tqdm(nlp.pipe(df['abstract'], n_process=n_processes), total=len(df)):
            token = [token.lemma_.lower() for token in abs if token.pos_ not in removal and not token.is_stop and token.is_alpha]
            tokens.append(token)
    else:
        for abs in tqdm(nlp.pipe(df['abstract'], n_process=n_processes), total=len(df)):
            token = [token.lower() for token in abs if token.pos_ not in removal and not token.is_stop and token.is_alpha]
            tokens.append(token)

    # # remove punctuation
    # print("Removing punctuation...")
    # df['cleaned_abstract'] = df['cleaned_abstract'].progress_apply(lambda x: re.sub(r'[^\w\s]', '', x))
    # # remove extra spaces
    # print("Removing extra spaces...")
    # df['cleaned_abstract'] = df['cleaned_abstract'].progress_apply(lambda x: re.sub(' +',' ',x))
    # if token:
    #     # remove stopwords
    #     print("Removing stopwords and creating tokens...")
    #     df['cleaned_abstract']=df['cleaned_abstract'].progress_apply(lambda x: ([word for word in x.split() if word not in sw_spacy]))
    #     if lemmatize:
    #         print("Lemmatizing and creating tokens...")
    #         df['cleaned_abstract'] = df['cleaned_abstract'].progress_apply(lambda x: ([token.lemma_ for token in list(nlp(x)) if (token.is_stop==False)]))
    # else:
    #     print("Removing stopwords...")
    #     df['cleaned_abstract'] = df['cleaned_abstract'].progress_apply(lambda x: ' '.join([token for token in x.split() if token not in sw_spacy]))
    #     if lemmatize:
    #         print("Lemmatizing...")
    #         df['cleaned_abstract'] = df['cleaned_abstract'].progress_apply(lambda x: ' '.join([token.lemma_ for token in list(nlp(x)) if (token.is_stop==False)]))

    df['cleaned_abstract'] = tokens
    
    return df




def ngram_filter(ngram):
    """
    Filter for n-grams with only noun-type structures

    Parameters
    ----------
    ngram : str
        Ngram to be filtered.
    
    Returns
    -------
    bool
        True if the ngram is not too common, False otherwise.
    """
    tag = nltk.pos_tag(ngram)
    if tag[0][1] not in ['JJ', 'NN'] and tag[1][1] not in ['NN']:
        return False
    if 'n' in ngram or 't' in ngram:
        return False
    if 'PRON' in ngram:
        return False
    return True


# Concatenate n-grams
def replace_ngram(x, bigrams=[], trigrams=[], fourgrams=[]):
    """
    Concatenates n-grams.

    Parameters
    ----------
    x : str
        Ngram to be concatenated.
    bigrams : list
        List of bigrams.
    trigrams : list
        List of trigrams.
    fourgrams : list   
        List of fourgrams.

    Returns
    -------
    str
    """
    if len(bigrams) != 0:
        for gram in bigrams:
            x = x.replace(gram, '_'.join(gram.split()))
        if len(trigrams) != 0:
            for gram in trigrams:
                x = x.replace(gram, '_'.join(gram.split()))
            if len(fourgrams) != 0:
                for gram in fourgrams:
                    x = x.replace(gram, '_'.join(gram.split()))         
    return x

def ngram(clean_abs, limit=1000, fourgrams=True):
    """
    Creates n-grams from the abstracts.

    Parameters
    ----------
    clean_abs : list
        List of abstracts.
    limit : int
        Maximum number of n-grams to be created.

    Returns
    -------
    list
        List of n-grams.
    """
    bigram_measures = nltk.collocations.BigramAssocMeasures()
    finder = nltk.collocations.BigramCollocationFinder.from_documents(clean_abs)
    # Filter only those that occur at least 50 times
    finder.apply_freq_filter(50)
    bigram_scores = finder.score_ngrams(bigram_measures.pmi)

    if len(bigram_scores) != 0:
        bigram_pmi = pd.DataFrame(bigram_scores)
        bigram_pmi.columns = ['bigram', 'pmi']
        bigram_pmi.sort_values(by='pmi', axis = 0, ascending = False, inplace = True)
        filtered_bigram = bigram_pmi[bigram_pmi.progress_apply(lambda bigram:\
                                                ngram_filter(bigram['bigram'])\
                                                and bigram.pmi > 5, axis = 1)]
        print("Bigrams: %s" % len(filtered_bigram))
        bigrams = [' '.join(x) for x in tqdm(filtered_bigram[:limit].bigram.values) if len(x[0]) > 2 or len(x[1]) > 2]
        del bigram_measures, finder, bigram_scores, bigram_pmi, filtered_bigram

        trigram_measures = nltk.collocations.TrigramAssocMeasures()
        finder = nltk.collocations.TrigramCollocationFinder.from_documents(clean_abs)
        # Filter only those that occur at least 30 times
        finder.apply_freq_filter(30)
        trigram_scores = finder.score_ngrams(trigram_measures.pmi)

        if len(trigram_scores) != 0:
            trigram_pmi = pd.DataFrame(trigram_scores)
            trigram_pmi.columns = ['trigram', 'pmi']
            trigram_pmi.sort_values(by='pmi', axis = 0, ascending = False, inplace = True)
            filtered_trigram = trigram_pmi[trigram_pmi.progress_apply(lambda trigram: \
                                                    ngram_filter(trigram['trigram'])\
                                                    and trigram.pmi > 5, axis = 1)]
            print("Trigrams: %s" % len(filtered_trigram))
            trigrams = [' '.join(x) for x in tqdm(filtered_trigram[:limit].trigram.values) if len(x[0]) > 2 or len(x[1]) > 2 and len(x[2]) > 2]
            del trigram_measures, finder, trigram_scores, trigram_pmi, filtered_trigram

            if fourgrams:
                fourgram_measures = nltk.collocations.QuadgramAssocMeasures()
                finder = nltk.collocations.QuadgramCollocationFinder.from_documents(clean_abs)
                # Filter only those that occur at least 10 times
                finder.apply_freq_filter(10)
                fourgram_scores = finder.score_ngrams(fourgram_measures.pmi)

                if len(fourgram_scores) != 0:
                    fourgram_pmi = pd.DataFrame(fourgram_scores)
                    fourgram_pmi.columns = ['fourgram', 'pmi']
                    fourgram_pmi.sort_values(by='pmi', axis = 0, ascending = False, inplace = True)
                    filtered_fourgram = fourgram_pmi[fourgram_pmi.progress_apply(lambda fourgram: \
                                                        ngram_filter(fourgram['fourgram'])\
                                                        and fourgram.pmi > 5, axis = 1)]
                    print("Fourgrams: %s" % len(filtered_fourgram))
                    fourgrams = [' '.join(x) for x in tqdm(filtered_fourgram[:limit].fourgram.values) if len(x[0]) > 2 or len(x[1]) > 2 and len(x[2]) > 2]
                    del fourgram_measures, finder, fourgram_scores, fourgram_pmi, filtered_fourgram

                print("Concatenating bigtams, trigrams, and fourgrams")
            
                return bigrams, trigrams, fourgrams 

            print('No fourgrams found')
        
            return bigrams, trigrams, [] 

        print('No trigrams found and fourgrams found')

        return bigrams, [], []

    print("No bigrams found")
    return [], [], []

def replace_with_ngrams(df, limit=1000, fourgrams=True):
    """
    Replaces the abstracts with n-grams.

    Parameters
    ----------
    df : dataframe
        Dataframe containing the abstracts.
    
    Returns
    -------
    dataframe
        Dataframe with the abstracts replaced with n-grams.
    list
        bigrams, trigrams, fourgrams = [], [], []
    """
    df['tmp_joined_cleaned_abstract'] = df['cleaned_abstract'].progress_apply(lambda x: ' '.join(x))
    # df['tmp_joined_cleaned_abstract'] = [' '.join(x) for x in tqdm(df.cleaned_abstract.values)]
    bigrams, trigrams, fourgrams = ngram(df.cleaned_abstract, limit=limit, fourgrams=fourgrams)
    df["n_gram_cleaned_abstract"] = df['tmp_joined_cleaned_abstract'].progress_apply(lambda x: replace_ngram(x, bigrams, trigrams, fourgrams))
    df['n_gram_cleaned_abstract'] = [word.split() for word in tqdm(df.n_gram_cleaned_abstract.values) if len(word) > 0]
    df.drop(["tmp_joined_cleaned_abstract"], axis=1, inplace=True)
    return df, bigrams, trigrams, fourgrams


def doc_relevance(df, query):
    df = basic_preprocessing(df)
    bm25 = BM25(df.cleaned_abstract)
    query = query.split()
    scores = bm25.get_scores(query)
    best_docs = sorted(range(len(scores)), key=lambda i: scores[i], reverse=True)
    best_docs = [b for b in best_docs if scores[b] > 0]
    print("Estimated number of relevant documents by a given search query is: ", len(best_docs))
    df = df.loc[best_docs]
    return df



def extract_data(topic_model, corpus, dictionary, doc_topic_dists=None):
    """
    Transforms the Gensim TopicModel and related corpus and dictionary into
    the data structures needed for the visualization.

    Parameters:
    ----------
    topic_model : gensim.models.ldamodel.LdaModel
        An already trained Gensim LdaModel.

    corpus : array-like list of bag of word docs in tuple form or scipy CSC matrix
        The corpus in bag of word form, the same docs used to train the model.
        The corpus is transformed into a csc matrix internally, if you intend to
        call prepare multiple times it is a good idea to first call
        `gensim.matutils.corpus2csc(corpus)` and pass in the csc matrix instead.
    For example: [(50, 3), (63, 5), ....]

    dictionary: gensim.corpora.Dictionary
        The dictionary object used to create the corpus. Needed to extract the
        actual terms (not ids).

    doc_topic_dist (optional): Document topic distribution from LDA (default=None)
        The document topic distribution that is eventually visualised, if you will
        be calling `prepare` multiple times it's a good idea to explicitly pass in
        `doc_topic_dist` as inferring this for large corpora can be quite
        expensive.

 
    Returns
    -------
    topic_term_dists : array-like, shape (`n_topics`, `n_terms`)
        Matrix of topic-term probabilities. Where `n_terms` is `len(vocab)`.

    doc_topic_dists : array-like, shape (`n_docs`, `n_topics`)
        Matrix of document-topic probabilities.

    doc_lengths : array-like, shape `n_docs`
        The length of each document, i.e. the number of words in each document.
        The order of the numbers should be consistent with the ordering of the
        docs in `doc_topic_dists`.

    vocab : array-like, shape `n_terms`
        List of all the words in the corpus used to train the model.

    term_frequency : array-like, shape `n_terms`
        The count of each particular term over the entire corpus. The ordering
        of these counts should correspond with `vocab` and `topic_term_dists`.

    """

    
    if not gensim.matutils.ismatrix(corpus):
        corpus_csc = gensim.matutils.corpus2csc(corpus, num_terms=len(dictionary))
    else:
        corpus_csc = corpus
        # Need corpus to be a streaming gensim list corpus for len and inference functions below:
        corpus = gensim.matutils.Sparse2Corpus(corpus_csc)
    

    vocab = list(dictionary.token2id.keys())
    
    beta = 0.01
    fnames_argsort = np.asarray(list(dictionary.token2id.values()), dtype=np.int_)
    term_freqs = corpus_csc.sum(axis=1).A.ravel()[fnames_argsort]
    term_freqs[term_freqs == 0] = beta
    doc_lengths = corpus_csc.sum(axis=0).A.ravel()

    assert term_freqs.shape[0] == len(dictionary),\
        'Term frequencies and dictionary have different shape {} != {}'.format(
        term_freqs.shape[0], len(dictionary))
    assert doc_lengths.shape[0] == len(corpus),\
        'Document lengths and corpus have different sizes {} != {}'.format(
        doc_lengths.shape[0], len(corpus))

    if hasattr(topic_model, 'lda_alpha'):
        num_topics = len(topic_model.lda_alpha)
    else:
        num_topics = topic_model.num_topics

    if doc_topic_dists is None:
        # If its an HDP model.
        if hasattr(topic_model, 'lda_beta'):
            gamma = topic_model.inference(corpus)
        else:
            gamma, _ = topic_model.inference(corpus)
        doc_topic_dists = gamma / gamma.sum(axis=1)[:, None]
    else:
        if isinstance(doc_topic_dists, list):
            doc_topic_dists = gensim.matutils.corpus2dense(doc_topic_dists, num_topics).T
        elif issparse(doc_topic_dists):
            doc_topic_dists = doc_topic_dists.T.todense()
        doc_topic_dists = doc_topic_dists / doc_topic_dists.sum(axis=1)

    assert doc_topic_dists.shape[1] == num_topics,\
        'Document topics and number of topics do not match {} != {}'.format(
        doc_topic_dists.shape[1], num_topics)

    # get the topic-term distribution straight from gensim without
    # iterating over tuples
    if hasattr(topic_model, 'lda_beta'):
        topic = topic_model.lda_beta
    else:
        topic = topic_model.state.get_lambda()
    topic = topic / topic.sum(axis=1)[:, None]
    topic_term_dists = topic[:, fnames_argsort]

    assert topic_term_dists.shape[0] == doc_topic_dists.shape[1]

    return topic_term_dists, doc_topic_dists, doc_lengths, vocab, term_freqs


def df_with_names(data, index_name, columns_name):
    if type(data) == pd.DataFrame:
        # we want our index to be numbered
        df = pd.DataFrame(data.values)
    else:
        df = pd.DataFrame(data)
    df.index.name = index_name
    df.columns.name = columns_name
    return df


def series_with_name(data, name):
    if type(data) == pd.Series:
        data.name = name
        # ensures a numeric index
        return data.reset_index()[name]
    else:
        return pd.Series(data, name=name)



def distance_between_topics(topic_term_dists, words, topic_proportion, n_components=2, start_index=0):
    """
    Parameters:
    -----------
    topic_term_dists: pd.DataFrame
        The topic-term distribution.

    words: list
        The words associated with each topic.

    topic_proportion: pd.Series
        The proportion of documents in each topic.

    start_index: int (default=0)
        The index to start calculating the distance.

    Returns:
    --------
    distance: pd.Series
        The distance between topics.
    
    """


    K = topic_term_dists.shape[0]
    embeddings = MinMaxScaler().fit_transform(topic_term_dists)
    embeddings = UMAP(n_neighbors=20, n_components=n_components, metric='hellinger', init='random').fit_transform(embeddings)

    if n_components == 2:
        return pd.DataFrame({   'x': embeddings[:, 0], 
                                'y': embeddings[:, 1],
                                'Words': words,
                                'Topic': range(start_index, K + start_index),
                                'Size': topic_proportion * 100})

    else:
        return pd.DataFrame({   'x': embeddings[:, 0], 
                                'y': embeddings[:, 1],
                                'z': embeddings[:, 2],
                                'Words': words,
                                'Topic': range(start_index, K + start_index),
                                'Size': topic_proportion * 100})



def preprocess_topics(  lda_model, 
                        corpus, 
                        dictionary, 
                        doc_topic_dists=None,
                        n_components=2
):
    """
    Visualize topics, their sizes, and their corresponding words
    This visualization is highly inspired by LDAvis, a great visualization
    technique typically reserved for LDA.

    Parameters:
    -----------
    topic_model : gensim.models.ldamodel.LdaModel
        An already trained Gensim LdaModel.

    corpus : array-like list of bag of word docs in tuple form or scipy CSC matrix
        The corpus in bag of word form, the same docs used to train the model.
        The corpus is transformed into a csc matrix internally, if you intend to
        call prepare multiple times it is a good idea to first call
        `gensim.matutils.corpus2csc(corpus)` and pass in the csc matrix instead.
    For example: [(50, 3), (63, 5), ....]

    dictionary: gensim.corpora.Dictionary
        The dictionary object used to create the corpus. Needed to extract the
        actual terms (not ids).

    doc_topic_dist (optional): Document topic distribution from LDA (default=None)
        The document topic distribution that is eventually visualised, if you will
        be calling `prepare` multiple times it's a good idea to explicitly pass in
        `doc_topic_dist` as inferring this for large corpora can be quite
        expensive.

    Returns:
    -------
    df

    """

    topic_term_dists, doc_topic_dists, doc_lengths, vocab, term_freqs = extract_data(   lda_model, 
                                                                                        corpus, 
                                                                                        dictionary, 
                                                                                        doc_topic_dists=None)

    topic_term_dist_cols = [
        pd.Series(topic_term_dist, dtype="float64")
        for topic_term_dist in topic_term_dists]

    topic_term_dists = pd.concat(topic_term_dist_cols, axis=1).T

    topics = lda_model.show_topics(num_topics=topic_term_dists.shape[0], num_words=10, formatted=False)
    words = [", ".join([word[0] for word in topic[1]]) for topic in topics]

    topic_term_dists = df_with_names(topic_term_dists, 'topic', 'term')
    doc_topic_dists = df_with_names(doc_topic_dists, 'doc', 'topic')
    term_frequency = series_with_name(term_freqs, 'term_frequency')
    doc_lengths = series_with_name(doc_lengths, 'doc_length')
    vocab = series_with_name(vocab, 'vocab')

    topic_freq = doc_topic_dists.mul(doc_lengths, axis="index").sum()
    # topic_freq  = np.dot(doc_topic_dists.T, doc_lengths)

    topic_proportion = (topic_freq / topic_freq.sum())

    topic_order = topic_proportion.index

    # reorder all data based on new ordering of topics
    topic_freq = topic_freq[topic_order]
    topic_term_dists = topic_term_dists.iloc[topic_order]
    doc_topic_dists = doc_topic_dists[topic_order]
    words = [words[order] for order in topic_order]

    df_topics = distance_between_topics(topic_term_dists, words, topic_proportion, n_components=n_components)

    return df_topics


def model_data_to_df(df, lda_model, corpus):
    """
    Converts the model data to a dataframe.

    Parameters:
    -----------
    lda_model : gensim.models.ldamodel.LdaModel
        An already trained Gensim LdaModel.
    
    corpus : array-like list of bag of word docs in tuple form
        The corpus in bag of word form, the same docs used to train the model.

    Returns:
    --------
    df : pd.DataFrame
        The dataframe containing the model data.
    """
    def sort_topics(tup):
        """
        Sorts the topics in descending order of probability.
        """
        tup.sort(key=lambda x: x[1], reverse=True)
        return tup

    # df["corpus"] = corpus
    topics = lda_model.show_topics(num_topics=len(lda_model.get_topics()), num_words=10, formatted=False)
    words = [", ".join([word[0] for word in topic[1]]) for topic in topics]
    topic_ids = []
    doc_topic_probabily = []

    for i in tqdm(range(len(corpus))):
            tp = sort_topics(lda_model[corpus[i]])
            topic_ids.append(tp[0][0])
            doc_topic_probabily.append(tp[0][1])
    df['topic_id'] = topic_ids
    df['doc_topic_probabily'] = doc_topic_probabily
    df['topic_keywords'] = df['topic_id'].apply(lambda x: words[x])

    # df['topic_ids'] = df['corpus'].progress_map(lambda x: (sort_topics(lda_model[x])[0][0]))
    # df['doc_topic_probabily'] = df['corpus'].progress_map(lambda x: (sort_topics(lda_model[x])[0][1]))
    # # df['topic_keywords'] = df['corpus'].progress_map(lambda x: words[sort_topics(lda_model[x])[0][0]])
    # df['topic_keywords'] = df['topic_ids'].progress_apply(lambda x: words[x])

    return df


def dynamic_topic_modelling(df, lda_model, num_words = 3):
    """
    Performs dynamic topic modelling on the dataframe.

    Parameters:
    -----------
    df : pd.DataFrame
        The dataframe containing the model data.
    
    lda_model : gensim.models.ldamodel.LdaModel
        An already trained Gensim LdaModel.
    
    num_words : int
        The number of words to be displayed for each topic.
    
    Returns:
    --------
    df : pd.DataFrame
        The dataframe containing the model data.

    """
    df = df.reset_index().rename(columns={'index': 'index_pos'})
    df_topic_weights = df[['topic_id', 'doc_topic_probabily', 'index_pos']]
    total_docs = df.groupby('year')['index_pos'].apply(lambda x: len(x.unique())).reset_index().rename(columns={'index_pos': 'total_docs'})
    df_avg = df.groupby(['year', 'topic_id']).agg({'doc_topic_probabily':'sum'}).reset_index()
    df_avg = df_avg.merge(total_docs, on='year', how="left")
    df_avg['average_weight'] = df_avg['doc_topic_probabily'] / df_avg['total_docs']
    topics = lda_model.show_topics(num_topics=len(lda_model.get_topics()), num_words=num_words, formatted=False)
    topic_id = [topic[0] for topic in topics]
    topic_labels = ["_".join([word[0] for word in topic[1]]) for topic in topics]
    data_tuple = list(zip(topic_id, topic_labels))
    df_labels = pd.DataFrame(data_tuple, columns = ['topic_id', 'topic_label'])
    df_labels['topic_label'] = df_labels['topic_id'].map(str) + "_" + df_labels['topic_label']
    df_avg = df_avg.merge(df_labels, on='topic_id')
    df = pd.merge(df,df_avg[['year', 'topic_id', 'average_weight', 'total_docs', 'topic_label']],on=['year', 'topic_id'], how='left')
    df = df[~df['year'].isin(['2004', '2005', '2006', '2007', '2008', '2011'])]
    df['year']=pd.to_datetime(df['year'], format='%Y-%m-%d')
    df = df.sort_values(by='year')


    return df